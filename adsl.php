<?php

# Copyright (c) 2011, 2014-2015, 2018, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("head.php");

print('<br/>');
print('<div class="well">');
print('<form action="https://tools.aquilenet.fr/cgi-bin/test.cgi" method="post">');
if ($a_adh >= 1) {
	print('<input type="hidden" name="tel" id="tel" value="'.$adh->phone.'"/>');
	print('<input type="hidden" name="cp" id="cp" value="'.$adh->$dolibarr_cp.'"/>');
	print('<input type="hidden" name="adherent" id="adherent" value="'.$adh->id.'"/>');
	if ($adh->morphy == "phy")
		$personne = "physique";
	else
		$personne = "morale";
	print('<input type="hidden" name="personne" id="personne" value="'.$personne.'"/>');
	print('<input type="hidden" name="prenom" id="prenom" value="'.$adh->$dolibarr_prenom.'"/>');
	print('<input type="hidden" name="nom" id="nom" value="'.$adh->$dolibarr_nom.'"/>');
	$adr = explode("\n",$adh->$dolibarr_adresse,2);
	print('<input type="hidden" name="adr" id="adr" value="'.trim($adr[0]).'"/>');
	print('<input type="hidden" name="adrbis" id="adrbis" value="'.trim($adr[1]).'"/>');
	print('<input type="hidden" name="ville" id="ville" value="'.$adh->$dolibarr_ville.'"/>');
	print('<input type="hidden" name="mail" id="mail" value="'.$adh->email.'"/>');
	if ($adh->type == "Membre préférentiel")
		print('<input type="hidden" name="tarif" id="tarif" value="preferentiel"/>');
	else
		print('<input type="hidden" name="tarif" id="tarif" value="particulier"/>');
}
if ($a_mandat >= 1) {
	print('<input type="hidden" name="iban" id="iban" value="'.$prelevement->iban.'"/>');
	print('<input type="hidden" name="bic" id="bic" value="'.$prelevement->bic.'"/>');
}
print('<p>Vous pouvez <input type="submit" class="btn btn-success btn-mini" id="submit" name="submit" value="commander"/> une ligne ADSL.</p>');
print('</form>');
print('</div>');

?>

<p>Pour toute information sur le services ADSL, consultez <a href=http://www.aquilenet.fr/node/8>la page ADSL du site web</a>.</p>

<p>Attention, tous ces chiffres sont en bps (bits par seconde). Pour avoir des Bps (Byte per second, octets par seconde), il faut diviser par 8.</p>

<?php

function print_graph($id, $login) {
	print("<div class='span9'>\n");
	print("<div class='tabbable tabs-left'>");
	print("<ul id='myTab".$id."' class='nav nav-tabs'>\n");
	print("<li class='nav-header'>Stats</li>\n");
	print("<li class='active'><a href='#day".$id."' data-toggle='tab'>Jour</a></li>\n");
	print("<li><a href='#week".$id."' data-toggle='tab'>Semaine</a></li>\n");
	print("<li><a href='#month".$id."' data-toggle='tab'>Mois</a></li>\n");
	print("<li><a href='#year".$id."' data-toggle='tab'>Année</a></li>\n");
	print("</ul>\n");
	print("<div id='myTabContent".$id."' class='tab-content'>\n");
	print("<div class='tab-pane fade in active' id='day".$id."'>\n");
	print("<a href=\"stats.php?login=$login\"><img src=\"stat.php?login=$login&freq=daily\"/></a>");
	print("</div>\n");
	print("<div class='tab-pane fade' id='week".$id."'>\n");
	print("<a href=\"stats.php?login=$login\"><img src=\"stat.php?login=$login&freq=weekly\"/></a>");
	print("</div>\n");
	print("<div class='tab-pane fade' id='month".$id."'>\n");
	print("<a href=\"stats.php?login=$login\"><img src=\"stat.php?login=$login&freq=monthly\"/></a>");
	print("</div>\n");
	print("<div class='tab-pane fade' id='year".$id."'>\n");
	print("<a href=\"stats.php?login=$login\"><img src=\"stat.php?login=$login&freq=yearly\"/></a>");
	print("</div>\n");
	print("</div>\n");
	print("</div>\n");
	print("</div>\n");
}

if ($lignes) {
	//print("<h2 class='page-header'>Lignes ADSL</h2>");
	foreach ($lignes as $ligne) {
		print ("<article>");
		$ligne = trim($ligne);
		$account = new Account($db);
		if ($account->fetch('', $ligne) >= 1) {
			print("<div class='row'>\n");
			print ("<h3 class='page-header'>".$account->label."</h3>\n");

			print("<div class='span3'>\n");
			print("<ul class='unstyled'>\n");
			$infos = explode("\n",$account->comment);
			print("<li>Débit : <code>".$infos[1]."</code></li>\n");
			print("<li>Tarif : <code>".$infos[2]."</code></li>\n");
			print("<li>Mensualités : <code>".number_format($infos[3], 2, ',', ' ')." €</code></li>\n");
			$login = $infos[7];
			$login = trim($login);
			print("<li>Login : <code>".$login."</code></li>\n");
			print("<li>Mot de passe : <code>".$infos[8]."</code></li>\n");
			print("<li>IPv4 : <code>".$infos[5]."</code></li>\n");
			print("<li>IPv6 : <code>".$infos[6]."</code></li>\n");
			print("</ul>\n");
			print("</div>\n");

			print_graph($account->id, $login . ".rrd");
			print("</div>\n");
		}
		print("</article>");
	}
} else {
	print("<div class='alert'>Vous n'avez pas de ligne ADSL</div>");
}

print("<div class='row'>\n");
print ("<h3 class='page-header'>Total FDN</h3>\n");

print("<div class='span3'>\n");
print("Ceci comptabilise l'ensemble du trafic de toutes les lignes ADSL de FDN. Cela permet d'avoir une idée de combien son propre trafic se retrouve mélangé avec le reste, et donc combien il contribue au coût de bande passante total pour FDN.");
print("</div>\n");

print_graph("Total", "total.rrd");
print("</div>\n");

print("<div class='row'>\n");
print ("<h3 class='page-header'>Moyenne FDN</h3>\n");

print("<div class='span3'>\n");
print("Ceci comptabilise une moyenne de l'ensemble du trafic de toutes les lignes ADSL de FDN, ramenée par abonné. Cela permet d'avoir une idée de la consommation usuelle des adhérents.");
print("</div>\n");

print_graph("Moyenne", "moyenne");
print("</div>\n");

print("<div class='row'>\n");
print ("<h3 class='page-header'>Histogramme FDN</h3>\n");

print("<div class='span3'>\n");
print("Ceci montre pour chaque débit mensuel moyen combien d'adhérents l'atteignent, donnant une idée de où on se place");
print("</div>\n");

print("<img src=\"stat.php?freq=histogram\"/>");
print("</div>\n");

require_once("tail.php");
?>
