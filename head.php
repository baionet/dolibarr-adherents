<?php require_once("dolibarr.php"); ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Aquilenet - Tools</title>
    <link rel=stylesheet type=text/css href="/bootstrap/css/bootstrap.css">
    <link rel=stylesheet type=text/css href="/bootstrap/css/jquery.ui.all.css">
  </head>

  <body>
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href='http://adherents.aquilenet.fr/'>Aquilenet</a>
          <ul class="nav">
            <li><a href="http://www.aquilenet.fr/" title="">Site web</a></li> 
            <li class="active"><a href="https://adherents.aquilenet.fr/" title="">Espace adhérent(e)</a></li>
            <li><a href="http://listes.aquilenet.fr" title="Listes de discussions">Listes</a></li> 
            <li><a href="https://webmail.aquilenet.fr/" title="">Webmail</a></li> 
            <li><a href="http://atelier.aquilenet.fr" title="Site de travail de l&#039;association">Atelier</a></li> 
            <li><a href="http://pastebin.aquilenet.fr" title="Outil en ligne de publication de texte">Pastebin</a></li> 
            <li><a href="http://www.aquilenet.fr/contact" title="">Contact</a></li> 
          </ul>
	  <ul class="nav pull-right">
	    <?php print("<li><a class='' href='#' title=''><i class='icon-user'></i> ".$nom."</a></li>"); ?>
	  </ul>
        </div>
      </div>
    </div>

    <div class="container">

<header>
<h2 class"page-header">Espace adhérent(e)</h2>
<br />
<ul class="nav nav-tabs">
<?php
$self = $_SERVER['PHP_SELF'];

function doprint($page, $self, $title) {
	if ($page == $self)
	      print("<li class='active'><a href=\"$page\" title=\"$title\">$title</a></li>");
	else
	      print("<li><a href=\"$page\" title=\"$title\">$title</a></li>");
}
doprint('/index.php', $self, "Accueil");
doprint('/infos.php', $self, "Fiche adhérent(e)");
doprint('/mail.php', $self, "Mail");
doprint('/adsl.php', $self, "ADSL");
doprint('/vpn.php', $self, "VPN");
doprint('/disque.php', $self, "Disque");
doprint('/compta.php', $self, "Compta");
doprint('http://dolibarr.aquilenet.fr/aquilenet/compta-asso.php', $self, "Compta Aquilenet");
?>
</ul>
</header>
