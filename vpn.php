<?php

# Copyright (c) 2011, 2014-2016, 2020-2021 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("head.php");

?>

<?php

print('<div class="well">');
print('<form action="https://tools.aquilenet.fr/cgi-bin/adhesion.cgi" method="post">');
if ($a_adh >= 1) {
	print('<input type="hidden" name="tel" id="tel" value="'.$adh->phone.'"/>');
	print('<input type="hidden" name="cp" id="cp" value="'.$adh->$dolibarr_cp.'"/>');
	print('<input type="hidden" name="adherent" id="adherent" value="'.$adh->id.'"/>');
	if ($adh->morphy == "phy")
		$personne = "physique";
	else
		$personne = "morale";
	print('<input type="hidden" name="personne" id="personne" value="'.$personne.'"/>');
	print('<input type="hidden" name="prenom" id="prenom" value="'.$adh->$dolibarr_prenom.'"/>');
	print('<input type="hidden" name="nom" id="nom" value="'.$adh->$dolibarr_nom.'"/>');
	$adr = explode("\n",$adh->$dolibarr_adresse,2);
	print('<input type="hidden" name="adr" id="adr" value="'.trim($adr[0]).'"/>');
	print('<input type="hidden" name="adrbis" id="adrbis" value="'.trim($adr[1]).'"/>');
	print('<input type="hidden" name="ville" id="ville" value="'.$adh->$dolibarr_ville.'"/>');
	print('<input type="hidden" name="mail" id="mail" value="'.$adh->email.'"/>');
	if ($adh->type == "Membre préférentiel")
		print('<input type="hidden" name="tarif" id="tarif" value="preferentiel"/>');
	else
		print('<input type="hidden" name="tarif" id="tarif" value="particulier"/>');
}
if ($a_mandat >= 1) {
	print('<input type="hidden" name="iban" id="iban" value="'.$prelevement->iban.'"/>');
	print('<input type="hidden" name="bic" id="bic" value="'.$prelevement->bic.'"/>');
}
print('<input type="hidden" name="vpn" id="vpn" value="1"/>');
print('<p>Vous pouvez <input type="submit" class="btn btn-success btn-mini" id="submit" name="submit" value="commander"/> un VPN.</p>');
print('</form>');
print('</div>');

print("<p>Une <a href='http://www.aquilenet.fr/content/vpn'>foire aux questions</a> est disponible sur le site web, et la <a href=https://atelier.aquilenet.fr/projects/aquilenet/wiki/Configuration_VPN>documentation de la configuration du VPN</a> est sur l'atelier.</p>");

print("<p>Attention, tous ces chiffres sont en Bps (Byte par seconde, octets par seconde). Pour avoir des bps (bits per second), il faut multiplier par 8.</p>");

if ($contrats) {
	//print("<h2 class='page-header'>Lignes ADSL</h2>");
	foreach ($contrats as $objp) {
		if (substr($objp->ref,0,4) != 'vpn:')
			continue;

		$login = substr($objp->ref,4);
		print("<article>");
		print("<div class='row'>\n");
		print("<h3 class='page-header'>".$objp->label."</h3>\n");
		print("<div class=span3'>\n");
		print("<ul class='unstyled'>\n");
		print("<li>Ouverture: ".dol_print_date($objp->ouverture)."</li>\n");
		print("<li>Prix: ".number_format($objp->prix, 2, ',', ' ')." €/mois</li>\n");
		print("<li>Login: ".$login."</li>");
		print("<li>Mot de passe: ".$objp->note."</li>");
		print("<li><a href='cube.php?login=".$login."'>Fichier cube</a></li>");
		print("</ul>\n");
		print("</div>\n");
		print("</div>\n");
		print("</article>");
	}
} else {
	print("<div class='alert'>Vous n'avez pas de contrat</div>");
}

print("<article>");
print("<div class='row'>");
print("<h3 class='page-header'>Statistiques de trafic total Aquilenet</h3>");

print("<div class='span3'>\n");
print("Ceci comptabilise l'ensemble du trafic de tous les VPNs d'Aquilenet. Cela permet d'avoir une idée de combien son propre trafic se retrouve mélangé avec le reste, et donc combien il contribue au coût de bande passante total pour Aquilenet.");
print("</div>\n");

print("<div class='span9'>\n");
print("<div class='tabbable tabs-left'>");
print("<ul id='myTab' class='nav nav-tabs'>\n");
print("<li class='nav-header'>Stats Iris</li>\n");
print("<li><a href='#hour' data-toggle='tab'>5 heures</a></li>\n");
print("<li class='active'><a href='#day' data-toggle='tab'>Jour</a></li>\n");
print("<li><a href='#week' data-toggle='tab'>Semaine</a></li>\n");
print("<li><a href='#month' data-toggle='tab'>Mois</a></li>\n");
print("<li><a href='#year' data-toggle='tab'>Année</a></li>\n");
print("</ul>\n");
print("<div id='myTabContent' class='tab-content'>\n");
print("<div class='tab-pane fade' id='hour'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=3&freq=0\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=2&freq=0\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade in active' id='day'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=3&freq=1\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=2&freq=1\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade' id='week'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=3&freq=2\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=2&freq=2\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade' id='month'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=3&freq=3\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=2&freq=3\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade' id='year'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=3&freq=4\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=iris&srv=Network-NRPE\"><img src=\"statVpn.php?host=iris&source=2&freq=4\"/></a>");
print("</div>\n");
print("</div>\n");
print("</div>\n");
print("</div>\n");

print("<div class='span9'>\n");
print("<div class='tabbable tabs-left'>");
print("<ul id='myTab' class='nav nav-tabs'>\n");
print("<li class='nav-header'>Stats Demeter</li>\n");
print("<li><a href='#hour2' data-toggle='tab'>5 heures</a></li>\n");
print("<li class='active'><a href='#day2' data-toggle='tab'>Jour</a></li>\n");
print("<li><a href='#week2' data-toggle='tab'>Semaine</a></li>\n");
print("<li><a href='#month2' data-toggle='tab'>Mois</a></li>\n");
print("<li><a href='#year2' data-toggle='tab'>Année</a></li>\n");
print("</ul>\n");
print("<div id='myTabContent' class='tab-content'>\n");
print("<div class='tab-pane fade' id='hour2'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=3&freq=0\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=2&freq=0\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade in active' id='day2'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=3&freq=1\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=2&freq=1\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade' id='week2'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=3&freq=2\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=2&freq=2\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade' id='month2'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=3&freq=3\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=2&freq=3\"/></a>");
print("</div>\n");
print("<div class='tab-pane fade' id='year2'>\n");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=3&freq=4\"/></a>");
print("<a href=\"http://athena.aquilenet.fr/pnp4nagios/graph?host=demeter&srv=Network-NRPE\"><img src=\"statVpn.php?host=demeter&source=2&freq=4\"/></a>");
print("</div>\n");
print("</div>\n");
print("</div>\n");
print("</div>\n");

print("</div>\n");
print("</article>");

require_once("tail.php");
?>
