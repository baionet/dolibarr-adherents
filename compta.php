<?php

# Copyright (c) 2011, 2013-2016, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("head.php");
?>

<?php if ($a_tiers >= 1): ?>
    <?php if ($adh->datefin < time()): ?>
    	<div class="alert alert-error">
    <?php else: ?>
    	<div class="alert alert-info">
    <?php endif; ?>
    <h4>Date de renouvellement d'adhésion</h4> 
    <?php print(dol_print_date($adh->datefin,'day')); ?>
    </div>
    <?php
    if ($a_prelevement >= 1) {
	print("<p>Autorisation de prélèvements / mandats : ".$prelevement->bank." ".$prelevement->code_banque." ".$prelevement->code_guichet." ".$prelevement->number." ".$prelevement->cle_rib."\n");
	print("<tt><pre>\n");
	print($prelevement->domiciliation);
	print("</pre></tt></p>");
    }

    if ($a_mandat >= 1) {
	print("<p>Compte : ".$prelevement->iban." ".$prelevement->bic."</p>\n");
    }

	print('<br/>');
	print('<div class="well">');
	print('<form action="https://tools.aquilenet.fr/cgi-bin/adhesion.cgi" method="post">');
	if ($a_adh >= 1) {
		print('<input type="hidden" name="tel" id="tel" value="'.$adh->phone.'"/>');
		print('<input type="hidden" name="cp" id="cp" value="'.$adh->$dolibarr_cp.'"/>');
		print('<input type="hidden" name="adherent" id="adherent" value="'.$adh->id.'"/>');
		if ($adh->morphy == "phy")
			$personne = "physique";
		else
			$personne = "morale";
		print('<input type="hidden" name="personne" id="personne" value="'.$personne.'"/>');
		print('<input type="hidden" name="prenom" id="prenom" value="'.$adh->$dolibarr_prenom.'"/>');
		print('<input type="hidden" name="nom" id="nom" value="'.$adh->$dolibarr_nom.'"/>');
		$adr = explode("\n",$adh->$dolibarr_adresse,2);
		print('<input type="hidden" name="adr" id="adr" value="'.trim($adr[0]).'"/>');
		print('<input type="hidden" name="adrbis" id="adrbis" value="'.trim($adr[1]).'"/>');
		print('<input type="hidden" name="ville" id="ville" value="'.$adh->$dolibarr_ville.'"/>');
		print('<input type="hidden" name="mail" id="mail" value="'.$adh->email.'"/>');
		print('<input type="hidden" name="firstsubmit" id="firstsubmit" value="yes"/>');
	}
	print('<input type="hidden" name="prelevement" id="prelevement" value="-1"/>');
	print('<p>Vous pouvez <input type="submit" class="btn btn-success btn-mini" id="submit" name="submit" value="ajouter ou changer"/> vos coordonnées bancaires pour nous permettre de prélever régulièrement.</p>');
	print('</form>');
	print('</div>');

    foreach ($mandats as $mandat) {
	print("<p>Mandat : RUM ".$mandat."</p>\n");
    }

    if ($a_mandat >= 1) {
	print('<br/>');
	print('<div class="well">');
	print('<form action="https://tools.aquilenet.fr/cgi-bin/adhesion.cgi" method="post">');
	if ($a_adh >= 1) {
		print('<input type="hidden" name="tel" id="tel" value="'.$adh->phone.'"/>');
		print('<input type="hidden" name="cp" id="cp" value="'.$adh->$dolibarr_cp.'"/>');
		print('<input type="hidden" name="adherent" id="adherent" value="'.$adh->id.'"/>');
		if ($adh->morphy == "phy")
			$personne = "physique";
		else
			$personne = "morale";
		print('<input type="hidden" name="personne" id="personne" value="'.$personne.'"/>');
		print('<input type="hidden" name="prenom" id="prenom" value="'.$adh->$dolibarr_prenom.'"/>');
		print('<input type="hidden" name="nom" id="nom" value="'.$adh->$dolibarr_nom.'"/>');
		$adr = explode("\n",$adh->$dolibarr_adresse,2);
		print('<input type="hidden" name="adr" id="adr" value="'.trim($adr[0]).'"/>');
		print('<input type="hidden" name="adrbis" id="adrbis" value="'.trim($adr[1]).'"/>');
		print('<input type="hidden" name="ville" id="ville" value="'.$adh->$dolibarr_ville.'"/>');
		print('<input type="hidden" name="mail" id="mail" value="'.$adh->email.'"/>');
		print('<input type="hidden" name="iban" id="iban" value="'.$prelevement->iban.'"/>');
		print('<input type="hidden" name="bic" id="bic" value="'.$prelevement->bic.'"/>');
	}
	print('<input type="hidden" name="prelevement" id="prelevement" value="-1"/>');
	print('<p>Vous pouvez <input type="submit" class="btn btn-success btn-mini" id="submit" name="submit" value="générer"/> un nouveau mandat.</p>');
	print('</form>');
	print('</div>');
    }

    ?>
    <?php if ($a_account):
        $sql = "SELECT b.rowid, b.datev as dv, b.label, b.amount";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= " WHERE b.fk_account=".$account->id;
	$sql.= " AND b.fk_account = ba.rowid";
	$sql.= $db->order("b.datev, b.datec", "ASC");
	$result = $db->query($sql);
	if ($result) {
	    $total = 0;
	    $num = $db->num_rows($result);
	    print("<p>Également <a href=compta-ledger.php>téléchargeable au format ledger</a> (expérimental).</p>");
	    print("<table class='table table-striped table-condensed table-hover'>\n");
	    print("<tr><th>Date</th><th>Objet</th><th style='text-align:right'>Débit</th><th style='text-align:right'>Crédit</th><th style='text-align:right'>Solde</th></tr>\n");
	    for ($i = 0; $i < $num; $i++) {
	        $obj = $db->fetch_object($result);
		print("<tr class=");print(fmod($i,2)?"'even'":"'odd'");print(">");
		print("<td>".dol_print_date($db->jdate($obj->dv))."</td>");
		print("<td>".$obj->label."</td>");
		if ($obj->amount < 0) {
		    print("<td style='text-align:right'>".number_format($obj->amount, 2, ',', ' ')." €</td><td></td>");
		} else {
		    print("<td></td><td style='text-align:right'>".number_format($obj->amount, 2, ',', ' ')." €</td>");
		}
	        $total = $total + $obj->amount;
		print("<td style='text-align:right'>".number_format($total, 2, ',', ' ')." €</td>");
		print("</tr>\n");
	    }
	    print("<tr class='solde'><td></td><td>Solde</td>");
	    if ($account->solde(1) < 0) {
		print("<td style='text-align:right'>".number_format($account->solde(1), 2, ',', ' ')." €</td>");
	    } else {
	        print("<td></td><td style='text-align:right'>".number_format($account->solde(1), 2, ',', ' ')." €</td>");
	    }
	    print("</tr></table>\n");
	}
    endif; ?>
<?php endif; ?>

<?php
require_once("tail.php");
?>
