<?php

# Copyright (c) 2011, 2013-2014, 2016-2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("head.php");

$crt = "/srv/letsencrypt/certs/mail.aquilenet.fr.crt";
?>

<div class="row">
<div class="span8">
  <h2 class="page-header">e-mail  <small><?php print($mail); ?></h2>
  <dl class="dl-horizontal">
    <dt>Nom d'utilisateur:
    <dd><code><?php print("$login"); ?>@aquilenet.fr</code> (oui, <b>avec</b> <code>@aquilenet.fr</code>, pour tous les services mail).
    <dt>imap(s):
    <dd><code>imap.aquilenet.fr</code> port 993, SSL/TLS, authentification par mot de passe normal.
    <dt>pop3(s):
    <dd><code>pop3.aquilenet.fr</code> port 995, SSL/TLS, authentification par mot de passe normal.
    <dt>smtp(s):
    <dd><code>smtp.aquilenet.fr</code> port 587, STARTTLS, authentification par mot de passe normal.
<dt>Certificats:
<dd>
<iframe src=https://mail.aquilenet.fr/certifs.php width="100%"/>
<!--
    <dt>Empreinte MD5:
    <dd><code><?php system("openssl x509 -fingerprint -md5 -in $crt -noout | sed -e 's/.*=//'") ?></code>
    <dt>Empreinte SHA1:
    <dd><code><?php system("openssl x509 -fingerprint -sha1 -in $crt -noout | sed -e 's/.*=//'") ?></code>
    <dt>Empreinte SHA256:
    <dd><code><?php system("openssl x509 -fingerprint -sha256 -in $crt -noout | sed -e 's/.*=//'") ?></code>
    <dt>Empreinte SHA512:
    <dd><code><?php system("openssl x509 -fingerprint -sha512 -in $crt -noout | sed -e 's/.*=//'") ?></code>
-->
  </dl>
</div>
<div class="span4">
<div class="well">
<h2>webmail</h2>
<p>Si vous le désirez, vous pouvez choisir d'utiliser le webmail disponible sur
<a href='https://webmail.aquilenet.fr'>https://webmail.aquilenet.fr</a>.</p>  
<p>L'identifiant et le mot de passe sont les mêmes que pour votre compte imap.</p>
<p>Un lien vers cette adresse est disponible dans la barre de menu, en haut de tout les sites aquilenet.fr.</p>
</div>
</div>
</div>

<?php
require_once("tail.php");
?>
