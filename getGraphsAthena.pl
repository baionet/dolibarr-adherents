#!/usr/bin/perl

use strict;
use WWW::Mechanize;

my $mech = WWW::Mechanize->new(
	agent => 'SI-AQN'
);

if(@ARGV != 3) {
	print "Usage: $0 host source view\n";
	exit 1;
}

my $host = $ARGV[0];
my $source = $ARGV[1];
my $view = $ARGV[2];

$mech->get("http://athena.aquilenet.fr/pnp4nagios/image?host=$host&srv=Network-NRPE&source=$source&view=$view", ':content_file' => '/dev/stdout');

exit 0;
