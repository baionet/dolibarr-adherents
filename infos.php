<?php

# Copyright (c) 2011-2014, 2017, 2020-2021 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("head.php");
?>

<?php if ($a_tiers >= 1 && $a_adh >= 1): ?>

<div class="row">
  <div class="span6">
  <h2 class="page-header">Informations</h2>
  <ul class="unstyled">
  <li>Nom : <code><?php print ($nom); ?></code><li>
  <li>Identifiant : <code><?php print ($login); ?></code><li>
  <li>Numéro d'adhérent(e) : <code><?php print($adh->id); ?></code></li>
  <li>Téléphone : <code><?php print($adh->phone); ?></code></li>
  <li>Email : <code><?php print($adh->email); ?></code></li>
  <li>Empreinte PGP : <code><?php print($tiers->siren.$tiers->siret.$tiers->ape.$tiers->idprof4); ?></code></li>
  <li>Adresse : </li>
  <pre><address><?php
  print($adh->$dolibarr_adresse."\n");
  print($adh->$dolibarr_cp." ".$adh->$dolibarr_ville);
  ?></address></pre>
  </ul>
  </div>
  <div class="span4 offset1 well">
    <fieldset><legend>Changement de mot de passe</legend>
    <?php 
    $passwd = $_POST['password'];
    $passwd2 = $_POST['password2'];
    if ($passwd || $passwd2) {
    	if ($passwd != $passwd2) {
           print("<div class='alert fade in'><a class='close' data-dismiss='alert' href='#'>×</a>Mots de passe différents</div>");
       } else {
           $min = preg_match("/[a-z]/", $passwd);
           $maj = preg_match("/[A-Z]/", $passwd);
           $num = preg_match("/[0-9]/", $passwd);
           $ok = preg_match("/^[0-9a-zA-Z]+$/", $passwd);
           if ($ok != 1) {
               print("<div class='alert fade in'><a class='close' data-dismiss='alert' href='#'>×</a>Le mot de passe ne doit contenir que des lettres de a à z non accentuées, des majuscules, des minuscules et des chiffres.</div>");
           } else if (strlen($passwd) < 8 || !$min || !$maj || !$num) {
               print("<div class='alert fade in'><a class='close' data-dismiss='alert' href='#'>×</a>Mot de passe trop simple : utilisez au moins 8 caractères, des majuscules, des minuscules et des chiffres.</div>");
           } else if (1) {
       	//$adh->pass = $passwd;
       	if ($adh->setPassword($user, "{crypt}".crypt($passwd)) >= 0) {
       	    print("<div class='alert alert-success'>Ok, mot de passe changé !</div>");
       	} else {
       	    print("<div class='alert alert-error'>Problème changement de mot de passe.</div>");
       	}
           }
       }
    }
    ?>
    <form method="post" action="infos.php">
      <label for="password">Nouveau mot de passe</label>
      <input type="password" name="password" />
      <label for="password">Confirmation</label>
      <input type="password" name="password2" />
      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Enregistrer">
        <input type="reset" class="btn" value="Annuler">
      </div>
    </form>
    </fieldset>
  </div>
</div>

<?php endif; ?>


<?
require_once("tail.php");
?>
