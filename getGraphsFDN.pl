#!/usr/bin/perl

# Recuperation des graphiques de consommation de FDN a partir des identifiants
# d'adherent(e) du FAI en marque blanche (depuis le 31/10/11, l'identification avec
# le login radius d'un ligne n'est plus possible).
#
# USAGE: ./% login
#
# Récupéré du SI LDN, adapté pour les besoins d'Aquilenet

use strict;
use WWW::Mechanize;

my $mech = WWW::Mechanize->new(
	agent => 'SI-AQN'
);

# Identifiants d'adherent(e) du FAI en marque blanche
require "./pass.pl";

if(@ARGV != 2) {
	print "Usage: $0 login {daily weekly monthly yearly histogram}\n";
	exit 1;
}

# Abonne concerne
my $login = $ARGV[0];
my $what = $ARGV[1];

eval {
	# Identification sur le SI de FDN
	$mech->get("https://vador.fdn.fr/adherents/index.cgi?login=$Pass::adh_login&passwd=$Pass::adh_pwd&do=yes");
};

if($@) {
	#print "<div id='error'>Le service semble indisponible actuellement,<br />la version des graphs consultés dernièrement est affichée.</div>";
	exit 1;
}

# Recuperation de l'ID de session dans l'URL de redirection (aucun cookie)
my $graphs_uri = $mech->uri();

# Il faut obligatoirement passer par cette page pour faire generer les graphs et
# s'eviter un "session invalide"
$graphs_uri =~ s/-in/-stats/;
$mech->get($graphs_uri);

if ($what eq "histogram") {
	$graphs_uri =~ s/adh-stats/draw-debits/;
} else {
	$graphs_uri =~ s/-stats/-statimg/;

	# Recuperation du graphe individuel

	$login =~ s/@/%40/;

	$graphs_uri .= "&img=$login&freq=$what";
}
	
$mech->get($graphs_uri, ':content_file' => "/dev/stdout");

exit 0;
