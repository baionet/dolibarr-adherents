<?php

# Copyright (c) 2011, 2014-2017 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

header("Content-Type: text/plain");
header("Content-Disposition: attachment; filename=aquilenet.cube");

require_once("dolibarr.php");

if ($contrats) {
	foreach ($contrats as $objp) {
		if (substr($objp->ref,0,4) != 'vpn:')
			continue;

		if (substr($objp->ref,4) == $_GET['login'])
		{
?>
{
  "server_name": "vpn-both.aquilenet.fr",
  "server_port": "1194",
  "server_proto": "udp",
  "ip6_net": "",
  "ip4_addr": "",
  "crt_server_ca": "-----BEGIN CERTIFICATE-----|MIIFXzCCA0egAwIBAgIIWghH0jA6Mh0wDQYJKoZIhvcNAQELBQAwSzEVMBMGA1UE|AxMMQ0EgQXF1aWxlbmV0MRIwEAYDVQQKEwlBcXVpbGVuZXQxETAPBgNVBAcTCEJv|cmRlYXV4MQswCQYDVQQGEwJGUjAiGA8yMDE3MTExMjEzMDgzNVoYDzIwMzcwMTEx|MTMwODUwWjBLMRUwEwYDVQQDEwxDQSBBcXVpbGVuZXQxEjAQBgNVBAoTCUFxdWls|ZW5ldDERMA8GA1UEBxMIQm9yZGVhdXgxCzAJBgNVBAYTAkZSMIICIjANBgkqhkiG|9w0BAQEFAAOCAg8AMIICCgKCAgEAzsWZhPpVRZACT9DGZ9hWdJ+9h1H5I0Iyawbj|IMajiDukYmvqdfMA8WrFGIEplzKsYeawQCNzbUIUFZmSxuWGezivmaXN7aTxTeOY|V8k15GDXPxq8GQCzS4CQ1nNEou89Lw0OJGdwWnjc41B9tHdnI204NAoGxDaoN8/5|JAmsdUA/gXUQ8y3XRe/c9u9w93kvE3baNu0xreRITyZ30jm8kPFa2fLseGOlPpHd|dJV32aCRpoG4YCw41TY/jrrbq3zY9YJ5r9bseMpNNHTqVt6D8g2aLPx6x5fgjcNe|JPBuAl5zUvCoPgglVRsGnsoYtVJ3dfliDpgCDRQ+QnEJK8REg7IMkN2vp0gwkm3o|2eJi3H/M9ctuez+c3AzPzad7yml1fo8Yb5PQ9eh9yWTowK2gT5sv50bNvnPwd/DC|5CHe/9hatT6NNOq7QqJoSTG0uqO06Pkhn2dxdegBoKd2cTWk3Q+zBSZ9cp72096L|SMj6hIzpJuMESHDsL+atdkeyH3hwByjmQQfL4ICNe0SmbRMjR0VUqhNJh4BU2NXG|MI8X0EFALAGvLm6B4BhH6W2yu5RG6hDLhIylYjuxZO1BQNm5Ti/OsqNfflEoQWT9|cqLS01a5b9aHCBmq+JVeMY1Q/P5dzzPFPU+qk67/3M5Xd5D/cyUy7Thgh+8MwAL7|LzDu2PkCAwEAAaNDMEEwDwYDVR0TAQH/BAUwAwEB/zAPBgNVHQ8BAf8EBQMDBwQA|MB0GA1UdDgQWBBRKaOx0r4+mPaUJkqRSE2qkPOFrqjANBgkqhkiG9w0BAQsFAAOC|AgEArupn5y8pnsI7zUKMLoIqrfTQvWjGsaHu2WIzGmSvARP3rcCPhcwosAH3O2wK|8qLuAMRJOo7jyz5uhKsB+Fns3gsiR8QtjYquOJm0C5tHAc3q1+OVCmQx/b4jCNxW|7C8gmBCwi/DId5TtIsXl/E5XjmQfh8+RDNRmfmVKz7Qzn9DOv2sSkBJNAqgen9rD|AD/7R1/U87YpWg5/FvCCHL8BeYzCEfnzC9XtPdVQetZS8wjhKt2KpemiTT36ludU|gVyX8uRPCyPLJojx4zjVAORHxNTGg8VYGrThtpO34oVq4nNVSsmsAu1HckytKxQT|XdCOHXxxyUc/+hQthLpZfpTqkMej/vJyZjGEKzRFGUObklFbNVJGtnLErK+HsYrD|kp7WjN4ATFIOK/1iZi4XJJhntAY4GTCmp9LpJ9xCHDntZav8XNZgCJIuOsMhsTNh|/jSxERQwmJk94AMZ1i8xJw8cHpLcxN9EiOvlP1038mjkPty8WxYNGzQH4UXR65dk|fsA82BrLp4e1BOStSpwN3jMxbnQ3AMPsMH9++FD/6vAt93rsygsH7xgSHX0Nz1AF|4Lj/BfweNR3pPZGsjiUMMwH8HtgNtPCyxJWkO3ROL80msZjlGrRDxN3YPZzwUBtX|Qej58ht57jYAq7UBFhQePLO+bsPYU506g3KaQhMpk7pf3SI=|-----END CERTIFICATE-----",
  "crt_client": "",
  "crt_client_key": "",
  "crt_client_ta": "",
<?php
  print('  "login_user": "'.substr($objp->ref,4).'",'."\n");
  print('  "login_passphrase": "'.$objp->note.'",'."\n");
?>
  "dns0": "185.233.100.100",
  "dns1": "185.233.100.101",
  "openvpn_rm": [
    "comp-lzo adaptive",
    "ns-cert-type server",
    "remote-cert-tls server",
    ""
  ],
  "openvpn_add": [
    "verify-x509-name *.aquilenet.fr name"
  ]
}
<?php
		}

	}
}


?>
