<?php

# Copyright (c) 2011, 2014-2015, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require_once("head.php");

?>

<div class='well'/>Pour commander de l'espace de stockage, contactez <a href=mailto:admin@aquilenet.fr>les administrateurs</a>.</div>

<?php


if ($contrats) {
	//print("<h2 class='page-header'>Lignes ADSL</h2>");
	foreach ($contrats as $objp) {
		if (substr($objp->ref,0,4) == 'vpn:')
			continue;

		print("<article>");
		print("<div class='row'>\n");
		print("<h3 class='page-header'>".$objp->label."</h3>\n");
		if (strstr($objp->label,"disque cloud"))
			print("<p>Disponible <a href=https://cloud.aquilenet.fr/>en ligne</a> ou via un outil owncloud, en lui donnant comme adresse de serveur <tt>cloud.aquilenet.fr</tt></p>");
		else if (strstr($objp->label,"disque NFS"))
			print("<p>Disponible en montage NFS sur votre VM. Nous contacter pour la configuration<p>");
		print("<div class=span3'>\n");
		print("<ul class='unstyled'>\n");
		print("<li>Ouverture: ".dol_print_date($objp->ouverture)."</li>\n");
		print("<li>Espace: ".$objp->qty." Go</li>\n");
		print("<li>Prix unitaire: ".number_format($objp->prix, 2, ',', ' ')." €/Go/an</li>\n");
		print("<li>Total: ".number_format($objp->qty * $objp->prix, 2, ',', ' ')." €/an</li>\n");

		print("</ul>\n");
		print("</div>\n");
		print("</div>\n");
		print("</article>");
	}
} else {
	print("<div class='alert'>Vous n'avez pas de contrat</div>");
}

require_once("tail.php");
?>
