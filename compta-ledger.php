<?php

# Copyright (c) 2011, 2013-2016 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

header("Content-Type: text/plain");
#header("Content-Disposition: attachment; filename=compta.ledger");

require_once("dolibarr.php");

if ($a_tiers >= 1):

    if ($a_account):
        $sql = "SELECT b.rowid, b.datev as dv, b.label, b.amount";
	$sql.= " FROM ".MAIN_DB_PREFIX."bank_account as ba";
	$sql.= ", ".MAIN_DB_PREFIX."bank as b";
	$sql.= " WHERE b.fk_account=".$account->id;
	$sql.= " AND b.fk_account = ba.rowid";
	$sql.= $db->order("b.datev, b.datec", "ASC");
	$result = $db->query($sql);
	if ($result) {
	    $total = 0;
	    $num = $db->num_rows($result);
	    for ($i = 0; $i < $num; $i++) {
	        $obj = $db->fetch_object($result);
	        $total = $total + $obj->amount;
		print(strftime("%Y/%m/%d", $db->jdate($obj->dv))." ".$obj->label."\n");
		print("    aquilenet\t".number_format($obj->amount, 2, '.', ' ')."\t");
		print("=".number_format($total, 2, '.', ' ')."\n");
		print("    adherent\t".number_format(-$obj->amount, 2, '.', ' ')."\t");
		print("=".number_format(-$total, 2, '.', ' ')."\n");
		print("\n");
	    }
	}
    endif;
endif;

?>
