<?php

# Copyright (c) 2011, 2013-2015, 2017-2018, 2020-2021 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Récupération infos LDAP
$login = $_SERVER["PHP_AUTH_USER"];
$nom = $_SERVER["AUTHENTICATE_CN"];
$mail = $_SERVER["AUTHENTICATE_MAIL"];
//print_r($_SERVER);

# Pour tricher
#if ($login == "samuel.thibault") {
#	$login = "jeannot.lapin";
#	$nom = "Jeannot Lapin";
#}

# Connection Dolibarr
require_once("/usr/share/dolibarr/htdocs/master.inc.php");
$langs->load("main");
@set_time_limit(0);
$user->getrights();

$dolibarr_version = preg_split('/[.-]/',DOL_VERSION);

require_once(DOL_DOCUMENT_ROOT."/lib/admin.lib.php");
require_once(DOL_DOCUMENT_ROOT."/adherents/class/adherent.class.php");
require_once(DOL_DOCUMENT_ROOT."/compta/bank/class/account.class.php"); 
require_once(DOL_DOCUMENT_ROOT."/societe/class/companybankaccount.class.php"); 

$dolibarr_prenom = "firstname";
$dolibarr_nom = "lastname";
$dolibarr_adresse = "address";
$dolibarr_cp = "zip";
$dolibarr_ville = "town";
$dolibarr_cotisation = "subscription";
$dolibarr_note = "note_private";
$dolibarr_note_suffix = "_private";
$dolibarr_fetch_lines = "fetch_lines";
if (versioncompare($dolibarr_version, array("3","4","0")) < 0) {
	# old names
        $dolibarr_prenom = "prenom"; 
        $dolibarr_nom = "nom";
	$dolibarr_adresse = "adresse";
        $dolibarr_cp = "cp";
        $dolibarr_ville = "ville";
        $dolibarr_cotisation = "cotisation";
        $dolibarr_note = "note";
        $dolibarr_note_suffix = "note";
        $dolibarr_fetch_lines = "fetch_lignes";
}

# Objets utiles
$tiers = new Societe($db);
$a_tiers = $tiers->fetch('', $nom);
$lignes = Null;
$mandats = Null;
$contrats = array();
$a_adh = 0; 
$a_compte = 0;
$a_prelevement = 0;
$a_mandat = 0;
$a_account = 0;
if ($a_tiers >= 1) {
	$adh = new Adherent($db);
	$a_adh = $adh->fetch('', '', $tiers->id);
	if ($a_adh >= 1) {
		if ($adh->$dolibarr_note) {
			$lignes = explode("\n",$adh->$dolibarr_note);
		}
		$account = new Account($db);
		$a_account = $account->fetch('', 41100000+$adh->id);
	}
	$prelevement = new CompanyBankAccount($db);
	$a_compte = $prelevement->fetch(0,$tiers->id);
	if ($a_compte >= 1)
	{
	       if ($prelevement->code_banque != "")
			$a_prelevement = 1;
	       if ($prelevement->iban != "")
			$a_mandat = 1;
	       if ($tiers->$dolibarr_note)
		       $mandats = explode("\n",$tiers->$dolibarr_note);
	}

	$sql = "SELECT c.rowid as cid, c.ref as ref, c.$dolibarr_note as note,";
	$sql.= " s.rowid as socid, s.nom as nom,";
	$sql.= " p.rowid as pid, p.label as label,";
	$sql.= " cd.rowid as cdid, cd.date_ouverture as ouverture, cd.qty, cd.subprice as prix";
	$sql.= " FROM ".MAIN_DB_PREFIX."contrat as c,";
	$sql.= " ".MAIN_DB_PREFIX."societe as s,";
	$sql.= " ".MAIN_DB_PREFIX."contratdet as cd";
	$sql.= " LEFT JOIN ".MAIN_DB_PREFIX."product as p ON cd.fk_product = p.rowid";
	$sql.= " WHERE";
	$sql.= " s.rowid = ".$tiers->id;
	$sql.= " AND c.rowid = cd.fk_contrat";
	$sql.= " AND c.fk_soc = s.rowid";
	$sql.= " AND s.entity = ".$conf->entity;
	$sql.= " AND cd.statut = 4";

	$resql = $db->query($sql);
	if ($resql)
	{
		$numr = $db->num_rows($resql);
		for ($i = 0; $i < $numr; $i++) {
			$objp = $db->fetch_object($resql);
			$contrats[] = $objp;
		}
	}
}

?>
